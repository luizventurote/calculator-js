var SCREEN = document.querySelector('#screen'),
    VALUE_1 = null,
    VALUE_2 = null,
    CURRENT_OPTION = null;

function addToScreen(value) {
  
  SCREEN.value = SCREEN.value + value;
  
  if(!CURRENT_OPTION)
     VALUE_1 = parseInt(SCREEN.value);
    else
      VALUE_2 = parseInt(SCREEN.value);
}

function cleanScreen() {
  
  SCREEN.value = '';
}

function resetValues() {
  CURRENT_OPTION = null;
  VALUE_1 = null;
  VALUE_2 = null;
}

function getResults() {
  
  var result = 0;
  
  switch(CURRENT_OPTION) {
    case "sum":
      result = optionSum();
        break;
    case "subtract":
      //result = optionSubtract();
        break;
    case "multiply":
      //result = optionMultiply();
        break;
    case "divide":
      //result = optionDivide();
        break;
  }
  
  resetValues();
  cleanScreen();
  addToScreen(result);
}

// Run options ------------------------------------------------

function rumOption(option) {
  
  if(option == 'result') {
    
     getResults();
    
  } else {
    
    if(option == 'clear') {
      
      cleanScreen();
      resetValues();
       
       } else {
         
         CURRENT_OPTION = option;
         cleanScreen();
       }
  }
}

// Features ------------------------------------------------

function optionSum() {
  return VALUE_1 + VALUE_2;
}

//  Buttom click ------------------------------------------------

var buttons = document.querySelectorAll('.btn'), i;

for (i = 0; i < buttons.length; ++i) {
  
  buttons[i].addEventListener('click', function(event) {
    
    if( this.classList.contains('number') ) {
      
      // Add number to screen
      addToScreen(this.innerText);
      
    } else {
      
      // Rum an option
      if( this.classList.contains('option') ) {
        
        rumOption(event.target.getAttribute('data-option'));
       
      }
    }
    
  });
}


